package com.mobaxe.airhockey.android;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.mobaxe.airhockey.AirHockey;

public class AndroidLauncher extends AndroidApplication {
    private static final String AD_UNIT_ID = "ca-app-pub-5727465082302443/6528153412";
    private static final String TEST_DEVICE_ID = "8E452640BC83C672B070CDCA8AB9B06B";
    private AdView adView;
    private View gameView;
    private RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new AirHockey(), config);

        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(AD_UNIT_ID);

        layout = new RelativeLayout(this);

        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        RelativeLayout.LayoutParams gameParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        gameParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        gameParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(TEST_DEVICE_ID).build();

        adView.loadAd(adRequest);

        gameView = initializeForView(new AirHockey(), config);

        layout.addView(gameView, gameParams);
        layout.addView(adView, adParams);

        setContentView(layout);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adView != null) {
            adView.pause();
        }

    }

    /**
     * Called before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
    }
}
