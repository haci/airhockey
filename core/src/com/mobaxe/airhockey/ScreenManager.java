package com.mobaxe.airhockey;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.IntMap;
import com.mobaxe.airhockey.MyScreens;

public final class ScreenManager {

	private static ScreenManager instance;
	private AirHockey game;
	private IntMap<Screen> screens;
	private Object data1, data2, data3;

	private ScreenManager() {
		screens = new IntMap<Screen>();
	}

	public static ScreenManager getInstance() {
		if (instance == null) {
			instance = new ScreenManager();
		}
		return instance;

	}

	public void initialize(AirHockey game) {
		this.game = game;
	}

	public void show(MyScreens screen) {

		if (game == null) {
			return;
		}
		if (!screens.containsKey(screen.ordinal())) {
			screens.put(screen.ordinal(), screen.getScreenInstance());
		}
		game.setScreen(screens.get(screen.ordinal()));
	}

	public void sendData(Object data1) {
		this.data1 = data1;
	}

	public void sendData(Object data1, Object data2) {
		this.data1 = data1;
		this.data2 = data2;
	}

	public void sendData(Object data1, Object data2, Object data3) {
		this.data1 = data1;
		this.data2 = data2;
		this.data3 = data3;
	}

	public Object getData1() {
		return data1;
	}

	public Object getData2() {
		return data2;
	}

	public Object getData3() {
		return data3;
	}

	public void dispose(MyScreens screen) {
		if (!screens.containsKey(screen.ordinal())) {
			return;
		}
		screens.remove(screen.ordinal()).dispose();
	}

	public void dispose() {
		for (Screen screen : screens.values()) {
			screen.dispose();
		}
		screens.clear();
		instance = null;
	}

}
