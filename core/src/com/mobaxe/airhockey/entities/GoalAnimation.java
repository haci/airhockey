package com.mobaxe.airhockey.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class GoalAnimation {

	private TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal("effects/animationImages.atlas"));
	private SpriteBatch batch = new SpriteBatch();
	private Animation animation;

	private float textureWidth = textureAtlas.getRegions().get(0).originalWidth;
	private float elapsedTime;

	public GoalAnimation() {
		animation = new Animation(0.2f, textureAtlas.getRegions());
	}

	public void render(float x, float y) {
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.begin();
		batch.draw(animation.getKeyFrame(elapsedTime, true), x, y);
		batch.end();
	}

	public float getTextureWidth() {
		return textureWidth;
	}

	public void setAnimationSpeed(float animationSpeed) {
		animation = new Animation(animationSpeed, textureAtlas.getRegions());
	}

	public void dispose() {
		textureAtlas.dispose();
		batch.dispose();
	}

}
