package com.mobaxe.airhockey;

import com.badlogic.gdx.Game;

public class AirHockey extends Game {

	@Override
	public void create() {
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.MAIN_MENU_SCREEN);
	}

	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
	}
}
