package com.mobaxe.airhockey.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.airhockey.MyScreens;
import com.mobaxe.airhockey.ScreenManager;
import com.mobaxe.airhockey.entities.CustomButton;
import com.mobaxe.airhockey.gameobjects.Ball;
import com.mobaxe.airhockey.gameobjects.Computer;
import com.mobaxe.airhockey.gameobjects.ComputerStatus;
import com.mobaxe.airhockey.gameobjects.EntityProperty;
import com.mobaxe.airhockey.gameobjects.Player;
import com.mobaxe.airhockey.helpers.AssetsLoader;
import com.mobaxe.airhockey.helpers.Box2DFactory;
import com.mobaxe.airhockey.utils.Constants;
import com.mobaxe.airhockey.utils.Utils;

public class GameScreen extends InputAdapter implements Screen {

	// Constant variables class defined here
	private Utils utils;
	/* Use Box2DDebugRenderer, which is a model renderer for debug purposes */
	private Box2DDebugRenderer debugRenderer;
	private Computer computerPlayer;
	private Player player;
	private Ball ball;
	// private ComputerMoves computerMoves;
	/* As always, we need a camera to be able to see the objects */
	private OrthographicCamera camera;
	/* Define a world to hold all bodies and simulate reactions between them */
	private World world;

	/*
	 * Used to define a mouse joint for a body. This point will track a
	 * specified world point.
	 */
	private MouseJoint mouseJoint;
	private MouseJointDef mouseJointDef;

	/* Store the position of the last touch or mouse click */
	private Vector3 touchPosition;
	@SuppressWarnings("unused")
	private Body rightGoalLine;
	@SuppressWarnings("unused")
	private Body leftGoalLine;
	@SuppressWarnings("unused")
	private Body rightUpLine;
	@SuppressWarnings("unused")
	private Body leftUpLine;
	@SuppressWarnings("unused")
	private Body leftLine;
	@SuppressWarnings("unused")
	private Body rightLine;
	@SuppressWarnings("unused")
	private Body cornerLineLeftUp;
	@SuppressWarnings("unused")
	private Body cornerLineLeftDown;
	@SuppressWarnings("unused")
	private Body cornerLineRightUp;
	@SuppressWarnings("unused")
	private Body cornerLineRightDown;
	private Body goalLineUp;
	@SuppressWarnings("unused")
	private Body goalLineDown;
	@SuppressWarnings("unused")
	private Body halfLine;
	private Body invisibleWalls;
	private int computerScore;
	private int playerScore;
	private SpriteBatch batch;
	private boolean scored = false;
	private boolean computerWin = false;
	private boolean playerWin = false;
	private Sound hitWalls;
	private Sound goalSound;
	private Sound gamerSound;
	private Sound startSound;
	private Sound youLoseSound;
	private Sound youWinSound;
	private Body clickedBody;
	private Body cmpScore;
	private Body plyrScore;
	private boolean gameOver = false;
	private boolean gamePaused = false;
	private String gameLevel;

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// debugRenderer.render(world, camera.combined);
		checkCollision();

		if (gameOver == false) {
			goalLineUp.setUserData(loadSprites("scoreboard"));
			world.step(1f / 60f, 6, 2);
		}
		if (gamePaused) {
			gamePaused = false;
			ScreenManager.getInstance().show(MyScreens.MAIN_MENU_SCREEN);
		}

		setComputersDefensiveOrOffensive();
		computerPlayer.render(ball);
		ball.render();

		// DRAW ALL THE SPRITES RELATED TO BODIES
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		drawGameObjects();
		batch.end();
	}

	public void setComputersDefensiveOrOffensive() {
		if (gameLevel.equals(CustomButton.EASY)) {
			if (ball.getaBody().getPosition().y > 0 && ball.getaBody().getPosition().y < 13) {
				computerPlayer.setCurrentStatus(ComputerStatus.OFFENSIVE);
			} else {
				computerPlayer.setCurrentStatus(ComputerStatus.DEFENSIVE);
			}
		} else if (gameLevel.equals(CustomButton.MEDIUM)) {
			if (ball.getaBody().getPosition().y > 0 && ball.getaBody().getPosition().y < 13) {
				computerPlayer.setCurrentStatus(ComputerStatus.OFFENSIVE);
			} else {
				computerPlayer.setCurrentStatus(ComputerStatus.DEFENSIVE);
			}
		} else if (gameLevel.equals(CustomButton.HARD)) {
			if (ball.getaBody().getPosition().y > 0 && ball.getaBody().getPosition().y < 13) {
				computerPlayer.setCurrentStatus(ComputerStatus.OFFENSIVE);
			} else {
				computerPlayer.setCurrentStatus(ComputerStatus.DEFENSIVE);
			}
		}
	}

	public void drawGameObjects() {
		// STADIUM IMAGE
		if (invisibleWalls.getUserData() != null && invisibleWalls.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) invisibleWalls.getUserData();
			sprite.setPosition(invisibleWalls.getPosition().x - sprite.getWidth() / 2, invisibleWalls.getPosition().y
					- sprite.getHeight() / 2);
			sprite.draw(batch);
		}
		// PLAYER IMAGE
		if (player.getaBody().getUserData() != null && player.getaBody().getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) player.getaBody().getUserData();
			sprite.setPosition(player.getaBody().getPosition().x - sprite.getWidth() / 2, player.getaBody()
					.getPosition().y - sprite.getHeight() / 2);
			sprite.draw(batch);
		}
		// BALL IMAGE
		if (ball.getaBody().getUserData() != null && ball.getaBody().getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) ball.getaBody().getUserData();
			sprite.setPosition(ball.getaBody().getPosition().x - sprite.getWidth() / 2, ball.getaBody().getPosition().y
					- sprite.getHeight() / 2);
			sprite.draw(batch);
		}
		// COMPUTER IMAGE
		if (computerPlayer.getaBody().getUserData() != null
				&& computerPlayer.getaBody().getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) computerPlayer.getaBody().getUserData();
			sprite.setPosition(computerPlayer.getaBody().getPosition().x - sprite.getWidth() / 2, computerPlayer
					.getaBody().getPosition().y - sprite.getHeight() / 2);
			sprite.draw(batch);
		}
		// COMPUTER SCORE IMAGE
		if (cmpScore.getUserData() != null && cmpScore.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) cmpScore.getUserData();
			sprite.setPosition(cmpScore.getPosition().x - sprite.getWidth() + 10f,
					cmpScore.getPosition().y - sprite.getHeight() + 4.5f);
			sprite.draw(batch);
		}
		// PLAYER SCORE IMAGE
		if (plyrScore.getUserData() != null && plyrScore.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) plyrScore.getUserData();
			sprite.setPosition(plyrScore.getPosition().x - sprite.getWidth() + 10f,
					plyrScore.getPosition().y - sprite.getHeight() - 1.3f);
			sprite.draw(batch);
		}
		// SCOREBOARD IMAGE
		if (goalLineUp.getUserData() != null && goalLineUp.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) goalLineUp.getUserData();
			sprite.setPosition(goalLineUp.getPosition().x - sprite.getWidth() - 2.0f, goalLineUp.getPosition().y
					- sprite.getHeight() - 10.5f);
			sprite.draw(batch);
		}

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(this);
		// Gdx.input.setCatchBackKey(true);
		gameLevel = (String) ScreenManager.getInstance().getData1();
		batch = new SpriteBatch();
		hitWalls = AssetsLoader.loadSound("sounds/hitWallSound.mp3");
		goalSound = AssetsLoader.loadSound("sounds/goalSound.mp3");
		gamerSound = AssetsLoader.loadSound("sounds/gamerSound.mp3");
		startSound = AssetsLoader.loadSound("sounds/startSound.mp3");
		youLoseSound = AssetsLoader.loadSound("sounds/youLoseSound.mp3");
		youWinSound = AssetsLoader.loadSound("sounds/youWinSound.mp3");
		gameOver = false;

		/*
		 * Create world with zero gravity vector and tell world that we want
		 * objects to sleep. This last value conserves CPU usage.
		 */
		world = new World(new Vector2(0, 0), true);
		createContactListener();
		/* Create renderer */
		debugRenderer = new Box2DDebugRenderer();

		// Create Utils class
		utils = new Utils();

		/*
		 * Define camera viewport. Box2D uses meters internally so the camera
		 * must be defined also in meters. We set a desired width and adjust
		 * height to different resolutions.
		 */
		camera = new OrthographicCamera(Constants.WIDTH, utils.getHeight());

		/*
		 * Instantiate the vector that will be used to store click/touch
		 * positions.
		 */
		touchPosition = new Vector3();

		/* Create the ball */
		ball = new Ball(world);
		ball.initEntity();
		ball.getaBody().setUserData(loadSprites("ball"));

		// Create the player1
		player = new Player(world);
		player.initEntity();
		player.getaBody().setUserData(loadSprites("player"));

		// Create the computer
		computerPlayer = new Computer(world);
		computerPlayer.initEntity();
		computerPlayer.getaBody().setUserData(loadSprites("computer"));

		/* Create the walls */
		invisibleWalls = Box2DFactory.createInvisibleWalls(world);
		invisibleWalls.setUserData(loadSprites("stad"));

		cmpScore = Box2DFactory.createScoreBoardComputer(world);
		cmpScore.setUserData(loadSprites("computerScore"));

		plyrScore = Box2DFactory.createScoreBoardPlayer(world);
		plyrScore.setUserData(loadSprites("playerScore"));

		leftLine = Box2DFactory.createLeftLine(world);
		halfLine = Box2DFactory.createHalfLine(world);
		rightGoalLine = Box2DFactory.createGoalLineRight(world);
		leftGoalLine = Box2DFactory.createGoalLineLeft(world);
		rightUpLine = Box2DFactory.createGoalLineRightUp(world);
		leftUpLine = Box2DFactory.createGoalLineLeftUp(world);
		rightLine = Box2DFactory.createRightLine(world);
		cornerLineLeftUp = Box2DFactory.createCornerLineLeftUp(world);
		cornerLineLeftDown = Box2DFactory.createCornerLineLeftDown(world);
		cornerLineRightUp = Box2DFactory.createCornerLineRightUp(world);
		cornerLineRightDown = Box2DFactory.createCornerLineRightDown(world);
		goalLineUp = Box2DFactory.createGoalLineDown(world);
		goalLineUp.setUserData(loadSprites("scoreboard"));
		goalLineDown = Box2DFactory.createGoalLineUp(world);

		/* Define the mouse joint. We use walls as the first body of the joint */
		createMouseJointDefinition(invisibleWalls);

		// computerMoves = new ComputerMoves();
	}

	private Sprite loadSprites(String spriteName) {

		if (computerScore == 3 || playerScore == 3) {
			if (spriteName.equals("scoreboard")) {
				Sprite imageSprites = null;
				if (computerWin) {
					imageSprites = new Sprite(AssetsLoader.loadTexture("scoreboardimages/youLose.png"));
				}
				if (playerWin) {
					imageSprites = new Sprite(AssetsLoader.loadTexture("scoreboardimages/youWin.png"));
				}
				imageSprites.setSize(16.2f, 11f);
				imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
				return imageSprites;
			}
			Timer.schedule(new Task() {
				@Override
				public void run() {
					computerScore = 0;
					playerScore = 0;
					gameOver = true;
				}
			}, 0.2f);
			Timer.schedule(new Task() {
				@Override
				public void run() {
					gamePaused = true;
				}
			}, 4f);

		}
		if (spriteName.equals("player")) {
			Sprite imageSprites = new Sprite(AssetsLoader.loadTexture("gameobjects/player.png"));
			imageSprites.setSize(3.5f, 3.5f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		if (spriteName.equals("computer")) {
			Sprite imageSprites = new Sprite(AssetsLoader.loadTexture("gameobjects/computer.png"));
			imageSprites.setSize(3.5f, 3.5f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		if (spriteName.equals("ball")) {
			Sprite imageSprites = new Sprite(AssetsLoader.loadTexture("gameobjects/ball.png"));
			imageSprites.setSize(2.5f, 2.5f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		if (spriteName.equals("stad")) {
			Sprite imageSprites = new Sprite(AssetsLoader.loadTexture("gameobjects/stad.png"));
			imageSprites.setSize(22f, 32f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		if (spriteName.equals("computerScore")) {
			Sprite imageSprites = null;
			if (computerScore == 0) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/zero.png"));
			}
			if (computerScore == 1) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/one.png"));
			}
			if (computerScore == 2) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/two.png"));
			}
			if (computerScore == 3) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/three.png"));
				playerWin = false;
				computerWin = true;
				youLoseSound.play();
			}
			imageSprites.setSize(1.5f, 2.4f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		if (spriteName.equals("playerScore")) {
			Sprite imageSprites = null;
			if (playerScore == 0) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/zero.png"));
			}
			if (playerScore == 1) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/one.png"));
			}
			if (playerScore == 2) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/two.png"));
			}
			if (playerScore == 3) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/three.png"));
				playerWin = true;
				computerWin = false;
				youWinSound.play();
			}
			imageSprites.setSize(1.5f, 2.4f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		return null;
	}

	@Override
	public void dispose() {
		debugRenderer.dispose();
		world.dispose();
		hitWalls.dispose();
		goalSound.dispose();
		gamerSound.dispose();
		startSound.dispose();
		youLoseSound.dispose();
		youWinSound.dispose();
	}

	public void createContactListener() {
		world.setContactListener(new ContactListener() {

			@Override
			public void beginContact(Contact contact) {
				if (contact.getFixtureB().getBody().getUserData() == ball.getaBody().getUserData()) {
					hitWalls.play();
				}
				if (contact.getFixtureA().getBody().getUserData() == ball.getaBody().getUserData()) {
					gamerSound.play();
				}
			}

			@Override
			public void endContact(Contact contact) {

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {

			}

		});
	}

	/**
	 * Creates the MouseJoint definition.
	 * 
	 * @param body
	 *            First body of the joint (i.e. ground, walls, etc.)
	 */
	private void createMouseJointDefinition(Body body) {
		mouseJointDef = new MouseJointDef();
		mouseJointDef.bodyA = body;
		mouseJointDef.collideConnected = true;
		mouseJointDef.maxForce = 5000f;
		mouseJointDef.dampingRatio = 0;
		mouseJointDef.frequencyHz = 100;

	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		/*
		 * Define a new QueryCallback. This callback will be used in
		 * world.QueryAABB method.
		 */
		QueryCallback queryCallback = new QueryCallback() {
			@Override
			public boolean reportFixture(Fixture fixture) {
				boolean testResult;

				/*
				 * If the hit point is inside the fixture of the body, create a
				 * new MouseJoint.
				 */
				if (testResult = fixture.testPoint(touchPosition.x, touchPosition.y)) {
					mouseJointDef.bodyB = fixture.getBody();
					if (fixture.getBody().getUserData() != null) {
						clickedBody = fixture.getBody();
						if (fixture.getBody().getUserData() instanceof EntityProperty) {
							EntityProperty touchedEntityProp = (EntityProperty) fixture.getBody().getUserData();
							if (touchedEntityProp != EntityProperty.PLAYER_LP)
								return false;
						}
					}
					mouseJointDef.target.set(player.getaBody().getPosition().x, player.getaBody().getPosition().y);
					mouseJoint = (MouseJoint) world.createJoint(mouseJointDef);
				}

				return testResult;
			}
		};

		/* Translate camera point to world point */
		camera.unproject(touchPosition.set(screenX, screenY, 0));

		/*
		 * Query the world for all fixtures that potentially overlap the touched
		 * point.
		 */
		world.QueryAABB(queryCallback, touchPosition.x, touchPosition.y, touchPosition.x, touchPosition.y);

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		/* Whether the input was processed */
		boolean processed = false;

		/* If a MouseJoint is defined, destroy it */
		if (mouseJoint != null) {
			world.destroyJoint(mouseJoint);
			mouseJoint = null;
			processed = true;
		}

		return processed;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		/* Whether the input was processed */
		boolean processed = false;

		/*
		 * If a MouseJoint is defined, update its target with current position.
		 */
		if (mouseJoint != null) {

			/* Translate camera point to world point */
			camera.unproject(touchPosition.set(screenX, screenY, 0));
			if (clickedBody.getUserData() != null) {
				if (clickedBody.getUserData() instanceof Sprite) {
					Sprite sprite = (Sprite) clickedBody.getUserData();
					if (sprite.toString().equals(player.getaBody().getUserData().toString())) {
						mouseJoint.setTarget(new Vector2(touchPosition.x, touchPosition.y));
					}
				}
			}

		}

		return processed;
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	public void checkCollision() {
		Body body = (Body) ball.getaBody();
		if (scored == false) {
			if (body.getPosition().y < -14) {
				goalSound.play();
				body.setUserData(null);
				scored = true;
				body.getWorld().destroyBody(body);
				computerScore++;
				cmpScore.setUserData(loadSprites("computerScore"));
				Timer.schedule(new Task() {
					@Override
					public void run() {
						scored = false;
						startSound.play();
					}
				}, 0.3f);
				Timer.schedule(new Task() {
					@Override
					public void run() {
						ball.getaBody().setUserData(loadSprites("ball"));
					}
				}, 1f);
				Timer.schedule(new Task() {
					@Override
					public void run() {
						ball.initEntity();
					}
				}, 0.2f);

			}
			if (body.getPosition().y > 14) {
				goalSound.play();
				body.setUserData(null);
				scored = true;
				body.getWorld().destroyBody(body);
				playerScore++;
				plyrScore.setUserData(loadSprites("playerScore"));
				Timer.schedule(new Task() {
					@Override
					public void run() {
						computerPlayer.getaBody().getWorld().destroyBody(computerPlayer.getaBody());
						computerPlayer.initEntity();
						computerPlayer.getaBody().setUserData(loadSprites("computer"));
						ball.initEntity();
						scored = false;
						ball.getaBody().setUserData(loadSprites("ball"));
						startSound.play();
					}
				}, 0.6f);
			}
		}
	}

	public OrthographicCamera getCamera() {
		return camera;
	}

}
