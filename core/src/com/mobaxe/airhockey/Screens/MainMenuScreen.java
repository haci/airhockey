package com.mobaxe.airhockey.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.airhockey.entities.CustomButton;
import com.mobaxe.airhockey.helpers.AssetsLoader;
import com.mobaxe.airhockey.utils.Utils;

public class MainMenuScreen implements Screen {

	private Stage stage;
	private Table table;
	private Table bgTable;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;
	private Utils util;

	private CustomButton gameLevelEasyButton;
	private CustomButton gameLevelMediumButton;
	private CustomButton gameLevelHardButton;

	public MainMenuScreen() {
		util = new Utils();
		stage = new Stage(new StretchViewport(util.getVirtualWidth(), util.getVirtualHeight()));
		table = new Table();
		bgTable = new Table();

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		// Gdx.input.setCatchBackKey(true);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {
		table.setFillParent(true);
		bgTable.setFillParent(true);

		gameLevelEasyButton = new CustomButton(CustomButton.EASY, true);
		table.add(gameLevelEasyButton).pad(0, 0, 20, 0);
		table.row();

		gameLevelMediumButton = new CustomButton(CustomButton.MEDIUM, true);
		table.add(gameLevelMediumButton).pad(0, 0, 20, 0);
		table.row();

		gameLevelHardButton = new CustomButton(CustomButton.HARD, true);
		table.add(gameLevelHardButton).pad(0, 0, 20, 0);
		table.row();

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);

		stage.addActor(bgTable);
		stage.addActor(table);
	}

	private void setBackgroundImage() {
		bgTexture = AssetsLoader.loadTexture("mainmenuimages/mainmenuBackground.png");

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(util.getVirtualWidth() - bgSprite.getWidth() / 2);
		bgSprite.setY(util.getVirtualHeight() - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
