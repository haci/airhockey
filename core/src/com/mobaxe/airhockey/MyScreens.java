package com.mobaxe.airhockey;

import com.badlogic.gdx.Screen;
import com.mobaxe.airhockey.Screens.GameScreen;
import com.mobaxe.airhockey.Screens.MainMenuScreen;

public enum MyScreens {

	MAIN_MENU_SCREEN {
		public Screen getScreenInstance() {
			return new MainMenuScreen();
		}
	},
	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
