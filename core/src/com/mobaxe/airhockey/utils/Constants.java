package com.mobaxe.airhockey.utils;

public class Constants {
	public static float PLAYERBALLRADIUS = 1.5f;
	private float computerBallRadius = 1.5f;
	private float ballRadius = 1;
	public static float OFFSET = 0.5f;
	public static float WIDTH = 20;

	public float getComputerBallRadius() {
		return computerBallRadius;
	}

	public void setComputerBallRadius(float computerBallRadius) {
		this.computerBallRadius = computerBallRadius;
	}

	public float getBallRadius() {
		return ballRadius;
	}

	public void setBallRadius(float ballRadius) {
		this.ballRadius = ballRadius;
	}

}
