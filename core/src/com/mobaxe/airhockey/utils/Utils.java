package com.mobaxe.airhockey.utils;

import com.badlogic.gdx.Gdx;

public class Utils {
	private static float height = 20 * (Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth());
	private static float halfWidth = Constants.WIDTH / 2 - Constants.OFFSET;
	private static float halfHeight = height / 2 - Constants.OFFSET;

	private float virtualWidth = 480;
	private float virtualHeight = 800;
	private float realWidth = Gdx.graphics.getWidth();
	private float realHeight = Gdx.graphics.getHeight();

	public float getHeight() {
		return height;
	}

	public float getHalfWidth() {
		return halfWidth;
	}

	public float getHalfHeight() {
		return halfHeight;
	}

	public float getVirtualWidth() {
		return virtualWidth;
	}

	public float getVirtualHeight() {
		return virtualHeight;
	}

	public float getRealWidth() {
		return realWidth;
	}

	public float getRealHeight() {
		return realHeight;
	}

}
