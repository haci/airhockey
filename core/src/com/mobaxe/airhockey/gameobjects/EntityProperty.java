package com.mobaxe.airhockey.gameobjects;

public enum EntityProperty {

	PLAYER_AI(1), PLAYER_LP(2), PLAYER_RP(3), BALL(4);
	
	private int entityType;
	
	EntityProperty(int entityType){
		this.entityType = entityType;
	}
	
	public int getEntityType() {
		return entityType;
	}

	public void setEntityType(int entityType) {
		this.entityType = entityType;
	}
}
