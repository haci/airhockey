package com.mobaxe.airhockey.gameobjects;

import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.airhockey.utils.Utils;

public class Ball extends GameEntity {
	Utils util = new Utils();

	public Ball(World world) {
		super(world, EntityProperty.BALL);
		playerRadius = 1;
	}

	public void render() {

		if (getaBody().getPosition().x + getPlayerRadius() + 0.1f >= util
				.getHalfWidth())
			getaBody().setLinearVelocity(
					getaBody().getLinearVelocity().x - 0.3f,
					getaBody().getLinearVelocity().y);
		if (getaBody().getPosition().x - getPlayerRadius() - 0.1f <= -util
				.getHalfWidth())
			getaBody().setLinearVelocity(
					getaBody().getLinearVelocity().x + 0.3f,
					getaBody().getLinearVelocity().y);
		if (getaBody().getPosition().y + getPlayerRadius() + 0.1f >= util
				.getHalfHeight()
				|| getaBody().getPosition().y - getPlayerRadius() - 0.1f <= -util
						.getHalfHeight())
			getaBody().setLinearVelocity(getaBody().getLinearVelocity().x,
					getaBody().getLinearVelocity().y + 0.3f);

	}

}
