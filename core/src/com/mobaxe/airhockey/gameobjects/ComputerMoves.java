package com.mobaxe.airhockey.gameobjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class ComputerMoves extends GameEntity {
	public ComputerMoves(World world, EntityProperty entityProperty) {
		super(world, entityProperty);
	}

	public void control(GameEntity gameEntity, Ball ball) {
		// System.out.println(ball.getaBody().getPosition());
		// player.getaBody().setTransform(ball.getaBody().getPosition().x,
		// player.getaBody().getPosition().y,45);
		if (ball.getaBody().getPosition().y < 0) {// rival field
			// player.getaBody().setLinearVelocity(0, -10);
			gameEntity.getaBody().setType(BodyType.KinematicBody);
			gameEntity.getaBody().setAngularVelocity(0);
			gameEntity.getaBody().applyLinearImpulse(new Vector2(-1, -1),
					new Vector2(5, 3), true);
			// player.getaBody().setType(BodyType.DynamicBody);
			// player.getaBody().applyLinearImpulse(0, -5,0,0, true);
		} else {

			gameEntity.getaBody().applyLinearImpulse(0, -5,
					ball.getaBody().getPosition().x,
					ball.getaBody().getPosition().y, true);
			// System.out.println("Position" + player.getaBody().getPosition());
		}
	}

}
