package com.mobaxe.airhockey.gameobjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.airhockey.utils.Utils;

public class Computer extends GameEntity {
	private Vector2 speed = new Vector2(0, -1);
	private Utils util = new Utils();
	private Vector2 destination = new Vector2();

	private ComputerStatus currentStatus = ComputerStatus.DEFENSIVE;

	public Computer(World world) {
		super(world, EntityProperty.PLAYER_AI);
	}

	/**
	 * @return the currentStatus
	 */
	public ComputerStatus getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * @param currentStatus
	 *            the currentStatus to set
	 */
	public void setCurrentStatus(ComputerStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public void render(Ball ball) {
		boolean shouldStop = false;
		Vector2 currentVelocity = getaBody().getLinearVelocity();

		if (currentStatus == ComputerStatus.DEFENSIVE) {
			destination.set(0, util.getHalfHeight());
			shouldStop = (getaBody().getPosition().y > util.getHalfHeight() - ball.getPlayerRadius()
					- getPlayerRadius());

		} else {
			destination.set(ball.getaBody().getPosition());
		}
		if (getaBody().getPosition().y - getPlayerRadius() <= 0
				|| getaBody().getPosition().y > util.getHalfHeight() - getPlayerRadius())
			currentVelocity.y = currentVelocity.y * -0.8f;
		if (getaBody().getPosition().x - getPlayerRadius() <= -util.getHalfWidth()
				|| getaBody().getPosition().x + getPlayerRadius() >= util.getHalfWidth())
			currentVelocity.x = currentVelocity.x * -0.8f;
		boolean increaseX = destination.x > getaBody().getPosition().x;
		boolean increaseY = destination.y > getaBody().getPosition().y;

		if (increaseX)
			currentVelocity.x = Math.min(currentVelocity.x + 1.2f, 11.0f);
		else
			currentVelocity.x = Math.max(currentVelocity.x - 1.2f, -11.0f);

		if (increaseY)
			currentVelocity.y = Math.min(currentVelocity.y + 1.2f, 11.0f);
		else
			currentVelocity.y = Math.max(currentVelocity.y - 1.2f, -11.0f);

		if (shouldStop) {
			currentVelocity.x = 0;
			currentVelocity.y = 0;
		}
		getaBody().applyForce(new Vector2(0, 100), ball.getaBody().getPosition(), true);

		setSpeed(currentVelocity.x, currentVelocity.y);
		updateSpeed(ball);

		if (util.getHeight() - getPlayerRadius() >= getaBody().getPosition().y) {
			currentVelocity.x = 0;
		}
	}

	private void updateSpeed(Ball ball) {
		getaBody().setLinearVelocity(speed);
		// getaBody().setTransform(new Vector2(0, 0), 10);
	}

	public void switchStatus() {
		if (currentStatus == ComputerStatus.DEFENSIVE)
			currentStatus = ComputerStatus.OFFENSIVE;
		else
			currentStatus = ComputerStatus.DEFENSIVE;
	}

	/**
	 * @return the speed
	 */
	public Vector2 getSpeed() {
		return speed;
	}

	/**
	 * @param speed
	 *            the speed to set
	 */
	public void setSpeed(float x, float y) {
		this.speed.x = x;
		this.speed.y = y;
	}

	public Vector2 getDestination() {
		return destination;
	}

	public void setDestination(Vector2 destination) {
		this.destination = destination;
	}
}
