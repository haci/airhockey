package com.mobaxe.airhockey.gameobjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.mobaxe.airhockey.ScreenManager;
import com.mobaxe.airhockey.entities.CustomButton;
import com.mobaxe.airhockey.helpers.Box2DFactory;

public class GameEntity extends CircleShape {
	// Our game table
	private World world;
	private Body aBody;

	// Radius of ball
	float playerRadius = 1.8f;
	private EntityProperty entityProperty;
	private FixtureDef entityFixture;
	private float frictionAI;
	private float restitutionAI;
	private float restitutionBALL;
	private String gameLevel;

	public GameEntity(World world, EntityProperty entityProperty) {
		this.world = world;
		this.entityProperty = entityProperty;
		gameLevel = (String) ScreenManager.getInstance().getData1();
		if (gameLevel.equals(CustomButton.EASY)) {
			frictionAI = 0.6f;
			restitutionAI = 1.5f;
			restitutionBALL = 0.6f;
		} else if (gameLevel.equals(CustomButton.MEDIUM)) {
			frictionAI = 0.4f;
			restitutionAI = 1.7f;
			restitutionBALL = 0.8f;
		} else {
			frictionAI = 0.1f;
			restitutionAI = 2f;
			restitutionBALL = 0.9f;
		}
	}

	public void initEntity() {
		Shape entityShape = Box2DFactory.createCircleShape(playerRadius);
		entityFixture = Box2DFactory.createFixture(entityShape, 0.1f, 2f, 0.90f, false);
		// entityFixture.filter.groupIndex = 1;

		BodyType entityBodyType = BodyType.DynamicBody;

		Vector2 startPosition = new Vector2();
		switch (entityProperty) {
		case PLAYER_AI:
			entityFixture = Box2DFactory.createFixture(entityShape, 0.7f, frictionAI, restitutionAI, false);
			// Box2DFactory.createFixture(shape, density, friction, restitution,
			// isSensor)
			entityFixture.filter.groupIndex = 1;
			startPosition.set(0, 11.5f);
			entityBodyType = BodyType.KinematicBody;
			break;
		case PLAYER_LP:
			entityFixture = Box2DFactory.createFixture(entityShape, 0.1f, 1f, 0.09f, false);
			entityFixture.filter.groupIndex = 1;
			startPosition.set(0, -10);
			break;
		case BALL:
			entityFixture = Box2DFactory.createFixture(entityShape, 0.1f, 1f, restitutionBALL, false);
			entityFixture.filter.groupIndex = -1;
			startPosition.set(0, 0);
			break;
		default:
			startPosition.set(0, 0);
			playerRadius = 1;
			break;
		}

		aBody = Box2DFactory.createBody(world, entityBodyType, entityFixture, startPosition);
		aBody.setUserData(entityProperty);
	}

	public Body getaBody() {
		return aBody;
	}

	public float getPlayerRadius() {
		return playerRadius;
	}

	public void setEntityProperty(EntityProperty entityProperty) {
		this.entityProperty = entityProperty;
	}

	public EntityProperty getEntityProperty() {
		return entityProperty;
	}

	public void setaBody(Body aBody) {
		this.aBody = aBody;
	}
}
