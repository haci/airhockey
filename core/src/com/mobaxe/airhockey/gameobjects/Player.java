package com.mobaxe.airhockey.gameobjects;

import com.badlogic.gdx.physics.box2d.World;

public class Player extends GameEntity {

	public Player(World world) {
		super(world, EntityProperty.PLAYER_LP);
	}

}
