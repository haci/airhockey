package com.mobaxe.airhockey.helpers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.airhockey.utils.Utils;

public class Box2DFactory {

	static Utils utils = new Utils();

	public static Body createBody(World world, BodyType bodyType, FixtureDef fixtureDef, Vector2 position) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(position);

		Body body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);
		fixtureDef.shape.dispose();

		return body;
	}

	public static Shape createBoxShape(Vector2 center, float angle) {
		PolygonShape boxShape = new PolygonShape();
		boxShape.setAsBox(utils.getHalfWidth(), utils.getHalfHeight(), center, angle);

		return boxShape;
	}

	public static Shape createChainShape(Vector2[] vertices) {
		ChainShape chainShape = new ChainShape();
		chainShape.createChain(vertices);
		return chainShape;
	}

	public static Shape createCircleShape(float radius) {
		CircleShape circleShape = new CircleShape();
		circleShape.setRadius(radius);

		return circleShape;
	}

	public static Shape createPolygonShape(Vector2[] vertices) {
		PolygonShape polygonShape = new PolygonShape();
		polygonShape.set(vertices);

		return polygonShape;
	}

	public static Shape createTriangleShape() {
		PolygonShape triangleShape = new PolygonShape();
		triangleShape.set(new Vector2[] { new Vector2(-utils.getHalfWidth(), -utils.getHalfHeight()),
				new Vector2(0, utils.getHalfHeight()), new Vector2(utils.getHalfWidth(), -utils.getHalfHeight()) });

		return triangleShape;
	}

	public static FixtureDef createFixture(Shape shape, float density, float friction, float restitution,
			boolean isSensor) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.isSensor = isSensor;
		fixtureDef.density = density;
		fixtureDef.friction = friction;
		fixtureDef.restitution = restitution;

		return fixtureDef;
	}

	public static Body createScoreBoardComputer(World world) {
		Vector2[] vertices = new Vector2[] { new Vector2(9.5f, 1), new Vector2(9.5f, 2) };

		Shape shape = createChainShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createScoreBoardPlayer(World world) {
		Vector2[] vertices = new Vector2[] { new Vector2(9.5f, -1), new Vector2(9.5f, -2) };

		Shape shape = createChainShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createInvisibleWalls(World world) {
		Utils utils = new Utils();
		Vector2[] vertices = new Vector2[] { new Vector2(-utils.getHalfWidth(), -utils.getHalfHeight()),
				new Vector2(utils.getHalfWidth(), -utils.getHalfHeight()),
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()),
				new Vector2(-utils.getHalfWidth(), utils.getHalfHeight()),
				new Vector2(-utils.getHalfWidth(), -utils.getHalfHeight()) };

		Shape shape = createChainShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createLeftLine(World world) {
		Vector2[] vertices = new Vector2[] { new Vector2(-utils.getHalfWidth(), -utils.getHalfHeight()),
				new Vector2(-utils.getHalfWidth(), utils.getHalfHeight()) };

		Shape shape = createChainShape(vertices);

		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);

		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createRightLine(World world) {

		Vector2[] rightLineVercites = new Vector2[] { new Vector2(0, -utils.getHalfWidth() + 9.5f),
				new Vector2(1 * utils.getHalfWidth() - 9.5f, -utils.getHalfHeight() - 14.5f),

		};
		Shape shape = createChainShape(rightLineVercites);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));

	}

	public static Body createHalfLine(World world) {

		Vector2[] halfLineVertices = new Vector2[] { new Vector2(0, -utils.getHalfHeight()),
				new Vector2(-2 * utils.getHalfWidth(), -utils.getHalfHeight()),

		};
		Shape shape = createChainShape(halfLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createGoalLineDown(World world) {

		Vector2[] goalLineDownVertices = new Vector2[] { new Vector2(-8, -utils.getHalfHeight() - 15f),
				new Vector2(-1.2f * utils.getHalfWidth(), -utils.getHalfHeight() - 15f),

		};
		Shape shape = createChainShape(goalLineDownVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createGoalLineUp(World world) {

		Vector2[] goalLineUpVertices = new Vector2[] { new Vector2(-8, -utils.getHalfHeight() + 15f),
				new Vector2(-1.2f * utils.getHalfWidth(), -utils.getHalfHeight() + 15f),

		};
		Shape shape = createChainShape(goalLineUpVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createGoalLineRight(World world) {

		Vector2[] rightLineVertices = new Vector2[] { new Vector2(0, -utils.getHalfHeight() - 14.5f),
				new Vector2(-2 * utils.getHalfWidth() + 12, -utils.getHalfHeight() - 14.5f),

		};
		Shape shape = createChainShape(rightLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createGoalLineLeft(World world) {

		Vector2[] LeftLineVertices = new Vector2[] {

		new Vector2(-12, -utils.getHalfHeight() - 14.5f),
				new Vector2(-2 * utils.getHalfWidth(), -utils.getHalfHeight() - 14.5f),

		};
		Shape shape = createChainShape(LeftLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createGoalLineRightUp(World world) {

		Vector2[] upRightLineVertices = new Vector2[] { new Vector2(0, -utils.getHalfHeight() + 14.5f),
				new Vector2(-2 * utils.getHalfWidth() + 12, -utils.getHalfHeight() + 14.5f),

		};
		Shape shape = createChainShape(upRightLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createGoalLineLeftUp(World world) {

		Vector2[] UpLeftLineVertices = new Vector2[] {

		new Vector2(-12, -utils.getHalfHeight() + 14.5f),
				new Vector2(-2 * utils.getHalfWidth(), -utils.getHalfHeight() + 14.5f), };
		Shape shape = createChainShape(UpLeftLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createCornerLineLeftUp(World world) {

		Vector2[] cornerLineLeftUp = new Vector2[] {

		new Vector2(-17f, -utils.getHalfHeight() + 14.5f),
				new Vector2(-2f * utils.getHalfWidth(), -utils.getHalfHeight() + 12.5f), };
		Shape shape = createChainShape(cornerLineLeftUp);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createCornerLineLeftDown(World world) {

		Vector2[] cornerLineLeftDown = new Vector2[] {

		new Vector2(-17f, -utils.getHalfHeight() - 14.5f),
				new Vector2(-2 * utils.getHalfWidth(), -utils.getHalfHeight() - 12.5f), };
		Shape shape = createChainShape(cornerLineLeftDown);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createCornerLineRightUp(World world) {

		Vector2[] cornerLineRightUp = new Vector2[] {

		new Vector2(-2, -utils.getHalfHeight() + 14.5f),
				new Vector2(-0 * utils.getHalfWidth(), -utils.getHalfHeight() + 12.5f), };
		Shape shape = createChainShape(cornerLineRightUp);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

	public static Body createCornerLineRightDown(World world) {

		Vector2[] cornerLineRightDown = new Vector2[] {

		new Vector2(0, -utils.getHalfHeight() - 12.5f),
				new Vector2(-0.21f * utils.getHalfWidth(), -utils.getHalfHeight() - 14.5f), };
		Shape shape = createChainShape(cornerLineRightDown);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef,
				new Vector2(utils.getHalfWidth(), utils.getHalfHeight()));
	}

}
