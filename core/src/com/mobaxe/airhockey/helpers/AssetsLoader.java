package com.mobaxe.airhockey.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;

public class AssetsLoader {

	private static Sound sound;

	public static Texture loadTexture(String filePath) {
		return new Texture(Gdx.files.internal(filePath));
	}

	public static Sound loadSound(String filePath) {
		sound = Gdx.audio.newSound(Gdx.files.internal(filePath));
		return sound;
	}
}