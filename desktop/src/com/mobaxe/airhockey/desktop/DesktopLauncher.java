package com.mobaxe.airhockey.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mobaxe.airhockey.AirHockey;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Air Hockey";
		config.useGL30 = false;
		config.width =480;
		config.height = 720;
		new LwjglApplication(new AirHockey(), config);
	}
}
